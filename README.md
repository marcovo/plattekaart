# [Plattekaart.nl](https://plattekaart.nl)

## Welkom!
Dit project bevat de broncode van de webapplicatie [plattekaart.nl](https://plattekaart.nl). Je kunt hier de techniek achter de applicatie bekijken of volgen, en desgewenst problemen melden of functionaliteitsverzoeken indienen.

## Wat is het?
Plattekaart.nl is een webapplicatie waarmee online topografische kaarten kunnen worden gedownload, met kaartdata gebaseerd op verschillende bronnen, waaronder Kadaster TOPRaster (RD-coördinaten), DTK Nordrhein-Westfalen en DTK Rheinland-Pfalz (UTM-coördinaten). Er kunnen ook routes en punten op de kaarten worden toegevoegd. Op basis van een route kunnen ook routetechnieken worden opgesteld en gedownload.

## Bouwen
De applicatie is geschreven in Typescript. Als je de applicatie zelf lokaal wilt bouwen om bijvoorbeeld een merge request te maken, kun je na `git clone` de applicatie bouwen in drie stappen:

    make up
    make install
    make vite  #(Blijft draaien in achtergrond)

De applicatie is daarna bereikbaar op `localhost:8000/index.html`.

## Serverside
De gehele applicatie draait in de browser van de bezoeker. De enige uitzondering zijn de statistieken: hiervoor is een klein PHP script aanwezig.

## Software-bibliotheken
Voor de bouw van deze applicatie wordt gebruik gemaakt van de volgende technologieën:
 - De werkkaart wordt gebouwd met [OpenLayers](https://openlayers.org/), met kaartdata van [Open street map](http://www.openstreetmap.org/copyright) en [Nominatim](https://nominatim.org/).
 - De topografische kaart PDF wordt opgebouwd met behulp van [jsPDF](https://github.com/MrRio/jsPDF).
 - Het genereren van routetechnieken in tekenstijl gebeurt met [svg2roughjs](https://github.com/fskpf/svg2roughjs), gebaseerd op [Rough.js](https://roughjs.com/).
 - Verder wordt er nog onder andere gebruik gemaakt van [TypeScript](https://www.typescriptlang.org/), [Vue.js](https://vuejs.org/), [Bootstrap](https://getbootstrap.com/) ([icons](https://icons.getbootstrap.com/)) en [jQuery](https://jquery.com/).

## Kruispuntenroute
Generating *Kruispuntenroute*s is done using a series of mathematical techniques. Here we briefly describe the two prime techniques.

### Side road snapping
*[src/RouteTechniques/Util/SideRoadSnapping.ts](src/RouteTechniques/Util/SideRoadSnapping.ts)*

This algorithm receives all roads in a crossroad drawing, and snaps them to regular directions.
E.g., when snapping to 8 directions of 45 degrees, the regular directions are 0, 45, 90, ... 315.

Of each road, the algorithm determines the `startBearing` as the bearing of the first 10 meters of the road, as well as the `minBearing` and `maxBearing` equal to the leftmost bearing and rightmost bearing, respectively, of the entire road as seen from the center point.
Each road is assigned a `preferredDirection`, which is the regular direction closest to the `startBearing`.
The second-closest regular direction is assigned as `alternativeDirection`.

Using the described data structure, a branch-and-bound algorithm determines the best distribution of roads among the directions.
Here, the minimized error is the sum of squares of the differences between the `startBearing` and assigned direction of each road.
The branch-and-bound algorithm considers all possible distributions of roads over preferred and alternative directions, considering a number of bounding conditions:
 - Proof that a distribution with lower error has already been found
 - Conclusion that one cannot do better than assigning all roads to the preferred directions
 - Concluding infeasibility if a single road cannot be assigned to neither preferred nor alternative direction

After assigning roads to directions, the road coordinates are clamped to the angle segment corresponding to the direction.

### Line offsetting (creating road drawings)
*[src/Util/PolylineOffset/PolylineOffset.ts](src/Util/PolylineOffset/PolylineOffset.ts)*

This algorithm receives a tree of side roads centered around the crossroad center, each node representing a route coordinate.
Given this tree, it computes a series of lines (straight and arcs) that together constitute the line offset of the given tree.

The algorithm is an adaption of these algorithms/approaches:
 - https://inria.hal.science/inria-00518005/document
 - https://github.com/jbuckmccready/CavalierContours

The algorithm is a generalization in the sense that it works for trees of polylines instead of one single polyline.
The algorithm is a specialization compared to the mentioned algorithms in the sense that only straigth lines are accepted as input (i.e., no arcs) and the input is specially crafted and hence assumed to be traversed counter-clockwise, to make some computations easier. Also, no segment stitching is performed.

An abstract algorithm description is provided in [PolylineOffset.ts](src/Util/PolylineOffset/PolylineOffset.ts).
