import {copyObject} from "../src/Util/functions.ts";

test('copyObject copies correctly', () => {
    expect(copyObject(null)).toStrictEqual(null);
    expect(copyObject(null, null)).toStrictEqual(null);

    expect(copyObject()).toStrictEqual({});
    expect(copyObject({})).toStrictEqual({});
    expect(copyObject({}, {})).toStrictEqual({});

    expect(copyObject({a: 4}, {b: 5})).toStrictEqual({a: 4, b: 5});

    expect(copyObject(
        {a: {b: 4}},
        {a: {b: 5}})
    ).toStrictEqual({a: {b: 5}});

    expect(copyObject(
        {a: {b: 4}},
        {a: null})
    ).toStrictEqual({a: null});

    expect(copyObject(
        {a: {b: 4}},
        {a: undefined})
    ).toStrictEqual({a: {b: 4}});

    expect(copyObject(
        {a: {b: 4}},
        {a: undefined})
    ).toStrictEqual({a: {b: 4}});

    let a = {b: 5};
    let c = {d: a, f: 2};
    let e = copyObject(c);
    a.b = 6;
    c.f = 3;
    expect(e).toStrictEqual({d: {b: 5}, f: 2});
});

// test('copyObject copies lists', () => {
//     expect(copyObject({a: [1, 2]})).toStrictEqual({a: [1, 2]});
//
//     expect(copyObject({a: [1, 2]}, {a: [3, 4]})).toStrictEqual({a: [3, 4]});
// });
