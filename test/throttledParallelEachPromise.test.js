import {throttledParallelEachPromise} from "../src/Util/functions.ts";

test('throttledParallelEachPromise resolves on empty generator', () => {
    let a = 0;
    return throttledParallelEachPromise(5, function*() {
        a++;
    }).then(() => {
        expect(a).toBe(1);
    })
});

test('throttledParallelEachPromise resolves on generator smaller than threads', () => {
    let a = 0;
    return throttledParallelEachPromise(5, function*() {
        for(let i=0; i<3; i++) {
            a++;
            yield new Promise((resolve, reject) => setTimeout(resolve) );
        }
    }).then(() => {
        expect(a).toBe(3);
    });
});

test('throttledParallelEachPromise resolves on generator larger than threads', () => {
    let a = 0;
    return throttledParallelEachPromise(3, function*() {
        for(let i=0; i<5; i++) {
            a++;
            yield new Promise((resolve, reject) => setTimeout(resolve) );
        }
    }).then(() => {
        expect(a).toBe(5);
    });
});

test('throttledParallelEachPromise stops on reject', () => {
    expect.assertions(2);

    let a = 0;
    return throttledParallelEachPromise(3, function*() {
        for(let i=0; i<5; i++) {
            a++;
            yield new Promise((resolve, reject) => setTimeout(resolve) );
        }
        a++;
        yield new Promise((resolve, reject) => setTimeout(reject) );
        for(let i=0; i<5; i++) {
            a++;
            yield new Promise((resolve, reject) => setTimeout(resolve) );
        }
    }).catch(() => {
        expect(a).toBeGreaterThanOrEqual(6);
        expect(a).toBeLessThanOrEqual(8);
    });
});

test('throttledParallelEachPromise resolves on one thread', () => {
    let a = 0;
    return throttledParallelEachPromise(1, function*() {
        for(let i=0; i<5; i++) {
            a++;
            yield new Promise((resolve, reject) => setTimeout(resolve) );
        }
    }).then(() => {
        expect(a).toBe(5);
    });
});
