import predictName from "../src/Util/NamePredictor";

test('namePredictor predicts correct based on two last', () => {
    expect(predictName(['Item 1', 'Item 2'])).toBe('Item 3');
    expect(predictName(['Item 1)', 'Item 2)'])).toBe('Item 3)');

    expect(predictName(['Item 1A', 'Item 1B'])).toBe('Item 1C');
    expect(predictName(['Item 1 A', 'Item 1 B'])).toBe('Item 1 C');
    expect(predictName(['Item 1-A', 'Item 1-B'])).toBe('Item 1-C');
    expect(predictName(['Item 1.A', 'Item 1.B'])).toBe('Item 1.C');
    expect(predictName(['Item 1_A', 'Item 1_B'])).toBe('Item 1_C');
    expect(predictName(['Item 1A)', 'Item 1B)'])).toBe('Item 1C)');

    expect(predictName(['Item 1A', 'Item 2B'])).toBe('Item 3'); // 2C?

    expect(predictName(['Item A1', 'Item A2'])).toBe('Item A3');

    expect(predictName(['Punt 1 - bench', 'Punt 2 - some trees'])).toBe('Punt 3 -');
    expect(predictName(['1 - bench', '2 - some trees'])).toBe('3 -');

    expect(predictName(['Bench - Punt 1', 'Some trees - Punt 2'])).toBe('- Punt 3');

    expect(predictName(['Punt 1', 'Punt 2 A', 'Punt 2 B', 'Punt 3'])).toBe('Punt 4');

    expect(predictName(['8', '9'])).toBe('10');
    expect(predictName(['9', '10'])).toBe('11');
    expect(predictName(['Y', 'Z'])).toBe('AA');
    expect(predictName(['Z', 'AA'])).toBe('AB');
    expect(predictName(['AA', 'AB'])).toBe('AC');
    expect(predictName(['AY', 'AZ'])).toBe('BA');
    expect(predictName(['ZY', 'ZZ'])).toBe('AAA');

    expect(predictName(['A3', 'B1'])).toBe('B2');
    expect(predictName(['C3d', 'D1a'])).toBe('D1b');
    expect(predictName(['15.qe.F', '16.a.A'])).toBe('16.a.B');
    expect(predictName(['2.5.2', '3.0.0'])).toBe('3.0.1');

    expect(predictName(['foo', 'bar'])).toBeNull();

    expect(predictName(['foo 5', 'foo 6', 'bar 7'])).toBe('bar 8');
});

test('namePredictor predicts correct based on single', () => {
    expect(predictName(['Item 1'])).toBe('Item 2');
    expect(predictName(['Item 2'])).toBe('Item 3');
    expect(predictName(['Item 2)'])).toBe('Item 3)');
    expect(predictName(['Item 13'])).toBe('Item 14');

    expect(predictName(['Point 1 - A bench'])).toBe('Point 1 - B bench');

    expect(predictName(['Point a) Start'])).toBe('Point b) Start');

    expect(predictName(['1'])).toBe('2');

    expect(predictName([])).toBeNull();
    expect(predictName(['test'])).toBeNull();
});

test('namePredictor predicts correct based on multiple', () => {
    expect(predictName([
        'Point 1',
        'Point 2',
        'Point 3',
        'Point 4',
        'Break',
    ])).toBe('Point 5');

    expect(predictName([
        'Point 1',
        'Point 2',
        'Point 3',
        'Point 4',
        'Break',
        'Point 5',
    ])).toBe('Point 6');

    expect(predictName([
        'Point 1',
        'Point 2',
        'Point 3',
        'Point 4',
        'Break',
        'Point 8',
    ])).toBe('Point 9');
});
