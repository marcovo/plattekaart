up:
	docker-compose up -d

build:
	docker-compose build

down:
	docker-compose down

sh:
	docker-compose exec webserver bash

install:
	docker-compose exec webserver npm install

vite:
	docker-compose exec webserver npm run dev

vite-build:
	rm -rf ./dist/*
	cp index.html plattekaart.html
	cp changelog.html plattekaart-changelog.html
	sed -i "s/http:\/\/localhost:5173\///g" ./plattekaart.html
	sed -i "s/http:\/\/localhost:5173\///g" ./plattekaart-changelog.html
	docker-compose exec -e PLATTEKAART_VERSION=${PLATTEKAART_VERSION} webserver npm run build
	rm ./plattekaart.html
	rm ./plattekaart-changelog.html
	mv dist/plattekaart.html dist/index.html
	mv dist/plattekaart-changelog.html dist/changelog.html

release-live:
	bash ./release.sh

release-test:
	bash ./release.sh -t
