import CoordinateSystem from "../Coordinates/CoordinateSystem";
import Coordinate from "../Coordinates/Coordinate";

export default interface MapImageProvider {
    readonly name: string;
    readonly title: string;

    getCopyright(): string;

    isCompatibleWithCoordinateSystem(coordinateSystem: CoordinateSystem<Coordinate>): boolean;

    getCompatibleCoordinateSystems(): CoordinateSystem<Coordinate>[];

    getDefaultCoordinateSystem(): CoordinateSystem<Coordinate>;

    downloadLegend();

    getBoundingPolygon<C extends Coordinate>(coordinateSystem: CoordinateSystem<C>): Promise<C[]|null>;
}
