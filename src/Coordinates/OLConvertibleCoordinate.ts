import Coordinate from "./Coordinate";

export default interface OLConvertibleCoordinate extends Coordinate {

    toOpenLayersCoordinate(): number[];

}
