import Coordinate from "./Coordinate";

export default interface OLConvertibleCoordinateSystem<C extends Coordinate> {
    fromOpenLayersCoordinate(source: number[]): C;
}
