import {Snap} from "ol/interaction.js";
import {Options, Result} from "ol/interaction/Snap.js";
import {Pixel} from "ol/pixel.js";
import {Coordinate as olCoordinate} from "ol/coordinate.js";
import OlMap from "ol/Map.js";
import {isMobile} from "../functions.js";

export class VertexEdgeSnap extends Snap {
    constructor(
        options: Options,
        private readonly vertexPixelTolerance = isMobile() ? 30 : 10,
    ) {
        options.pixelTolerance = Infinity;
        super(options);
    }

    snapTo(pixel: Pixel, pixelCoordinate: olCoordinate, map: OlMap): Result | null {
        // Snap to edges
        this.vertex_ = false;
        this.edge_ = true;
        const edgeResult = super.snapTo(pixel, pixelCoordinate, map);

        if (!edgeResult) {
            return null;
        }

        // Snap the edge-snapped point to vertices
        this.vertex_ = true;
        this.edge_ = false;
        const vertexResult = super.snapTo(edgeResult.vertexPixel, edgeResult.vertex, map);

        if (!vertexResult) {
            return null;
        }

        // If the cursor or edge-snapped point is close to a vertex, snap to vertex
        // Note: This is aimed at snapping to one of the two vertices of the snapped edge, however
        // there is an edge case in the situation an edge separates between the cursor and a vertex,
        // in which case you would expect the edge to be snapped but instead the vertex is snapped
        // if it is within the pixel tolerance of the edge
        const vertexDistanceSquared = (vertexResult.vertexPixel[0] - edgeResult.vertexPixel[0]) ** 2 + (vertexResult.vertexPixel[1] - edgeResult.vertexPixel[1]) ** 2;
        if (vertexDistanceSquared < this.vertexPixelTolerance ** 2) {
            return vertexResult;
        }

        // We are not close to a vertex, so snap to edge
        return edgeResult;
    }
}
