import OlMap from 'ol/Map';
import {Vector as VectorSource} from "ol/source";
import {Vector as VectorLayer} from "ol/layer";
import {ArcSegment, Drawing, LineSegment} from "./DrawingGeometry";
import {LineString} from "ol/geom";
import {Feature} from "ol";

export class MapDrawer {
    private lastDrawingSet: Drawing[] = [];
    private newDrawingSet: Drawing[] = [];

    private readonly source: VectorSource;
    private readonly layer: VectorLayer<VectorSource>;

    constructor(readonly map: OlMap) {
        this.source = new VectorSource();
        this.layer = new VectorLayer({
            source: this.source,
        });

        map.addLayer(this.layer);

        map.on('change', () => {
            if (this.lastDrawingSet !== this.newDrawingSet) {
                this.draw();
            }
        });
    }

    public setDrawingSet(drawingSet: Drawing[]): void {
        this.newDrawingSet = drawingSet;
        this.map.changed();
    }

    private draw(): void {
        this.source.clear();

        for (const drawing of this.newDrawingSet) {
            this.drawDrawing(drawing);
        }

        this.lastDrawingSet = this.newDrawingSet;
    }

    private drawDrawing(drawing: Drawing): void {
        for (const polyline of drawing.polylines) {
            for (const segment of polyline.segments) {
                if (segment instanceof LineSegment) {
                    this.drawLineSegment(segment);
                } else if (segment instanceof ArcSegment) {
                    for (const segmentPiece of segment.discretize(10)) {
                        this.drawLineSegment(segmentPiece);
                    }
                }
            }
        }
    }

    private drawLineSegment(segment: LineSegment): void {
        this.source.addFeature(new Feature({
            geometry: new LineString([
                [segment.start.x, segment.start.y],
                [segment.end.x, segment.end.y],
            ]),
        }));
    }
}
