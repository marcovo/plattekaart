import {Serialization} from "../Main/Serializer";

export default interface Action {
    apply(initial: boolean): void;

    revert(): void;

    merge(newAction: Action): boolean;

    serializeForDebug(): Serialization;
}
