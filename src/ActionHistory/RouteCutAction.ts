import Action from "./Action";
import Route from "../Main/Route";
import {Serialization} from "../Main/Serializer";
import {Coordinate as olCoordinate} from "ol/coordinate";
import predictName from "../Util/NamePredictor.js";
import RouteCoordinateMutationAction from "./RouteCoordinateMutationAction.js";

export default class RouteCutAction extends RouteCoordinateMutationAction {

    public static create(route: Route, coordinate: olCoordinate): RouteCutAction|null {
        const result = route.locateCoordinate(coordinate);

        if (result === null) {
            return null;
        }

        if (result[0] === result[1]) {
            if (result[0] === 0 || result[0] === route.getCoordinates().length - 1) {
                return null;
            }

            return new RouteCutAction(route, result[0], null);
        } else {
            return new RouteCutAction(route, result[0], coordinate);
        }
    }

    readonly positionInRouteCollection: number;
    readonly route2: Route;

    private constructor(readonly route1: Route, edgeStartVertex: number, edgeCoordinate: olCoordinate|null) {
        // Copy route1
        const route2 = route1.clone();

        // Update route1
        const coordinates1 = route1.getCoordinates();
        if (edgeCoordinate == null) {
            coordinates1.splice(edgeStartVertex + 1, coordinates1.length - edgeStartVertex);
        } else {
            coordinates1.splice(edgeStartVertex + 1, coordinates1.length - edgeStartVertex, edgeCoordinate);
        }
        route1.getGeometry().setCoordinates(coordinates1);
        super(route1, route1.getOldCoordinates(), false, edgeCoordinate == null);

        // Track position
        this.positionInRouteCollection = route1.routeCollection.getRoutes().indexOf(route1);

        if (this.positionInRouteCollection === -1) {
            throw new Error('Invalid position');
        }

        // Update route2
        route2.setColor(route2.routeCollection.userInterface.newColor());
        let newName = predictName([route1.getName()]);
        for (const otherRoute of route1.routeCollection.getRoutes()) {
            if (otherRoute.getName() == newName) {
                newName = route1.getName() + ' (#2)';
                break;
            }
        }
        route2.setName(newName);

        const coordinates2 = route2.getCoordinates();
        let trimIntermediates;
        if (edgeCoordinate == null) {
            coordinates2.splice(0, edgeStartVertex);
            trimIntermediates = edgeStartVertex;
        } else {
            coordinates2.splice(0, edgeStartVertex + 1, edgeCoordinate);
            trimIntermediates = edgeStartVertex + 1;
        }
        if (route2.getIntermediates()) {
            for (const index in route2.getIntermediates().intermediates) {
                if (index < trimIntermediates) {
                    delete route2.getIntermediates().intermediates[index];
                }
            }
        }
        route2.applyCoordinatesChange(coordinates2, 0, - edgeStartVertex);
        if (route2.getIntermediates()) {
            route2.getIntermediates().updateIntermediateNumbers();
        }
        this.route2 = route2;
    }

    public getNewRoute(): Route {
        return this.route2;
    }

    public apply(initial: boolean) {
        super.apply(initial);

        this.route2.routeCollection.attachRoute(this.route2, this.positionInRouteCollection + 1);
    }

    public revert() {
        this.route2.routeCollection.detachRoute(this.route2);

        super.revert();
    }

    public merge(newAction: Action): boolean {
        return false;
    }

    public serializeForDebug(): Serialization {
        return {
            route1: this.route1.id,
            route2: this.route2.id,
            positionInRouteCollection: this.positionInRouteCollection,
        };
    }
}
