import Action from "./Action";
import Route from "../Main/Route";
import RouteIntermediates from "../Main/RouteIntermediates";
import {Serialization} from "../Main/Serializer";

export default class RouteRemoveIntermediatesAction implements Action {

    private readonly intermediates: RouteIntermediates;

    constructor(readonly route: Route) {
        this.intermediates = route.getIntermediates();
    }

    public apply() {
        this.route.getIntermediates().removeFromMap();
        this.route.setIntermediates(null);
    }

    public revert() {
        this.route.setIntermediates(this.intermediates);
        this.route.getIntermediates().addToMap();
    }

    public merge(newAction: Action): boolean {
        return false;
    }

    public serializeForDebug(): Serialization {
        return {
            route: this.route.id,
        };
    }
}
