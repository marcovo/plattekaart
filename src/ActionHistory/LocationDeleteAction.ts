import Action from "./Action";
import Location from "../Main/Location";
import {Serialization} from "../Main/Serializer";

export default class LocationDeleteAction implements Action {

    readonly position: number;

    constructor(readonly location: Location) {
        this.position = location.locationCollection.getLocations().indexOf(location);

        if(this.position === -1) {
            throw new Error('Invalid position');
        }
    }

    public apply() {
        this.location.locationCollection.detachLocation(this.location);
    }

    public revert() {
        this.location.locationCollection.attachLocation(this.location, this.position);
    }

    public merge(newAction: Action): boolean {
        return false;
    }

    public serializeForDebug(): Serialization {
        return {
            location: this.location.id,
            position: this.position,
        };
    }
}
