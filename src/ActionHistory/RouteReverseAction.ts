import Action from "./Action";
import Route from "../Main/Route";
import {Serialization} from "../Main/Serializer";

export default class RouteReverseAction implements Action {

    constructor(readonly route: Route) {
    }

    public apply() {
        this.route.doReverse();
    }

    public revert() {
        this.route.doReverse();
    }

    public merge(newAction: Action): boolean {
        return false;
    }

    public serializeForDebug(): Serialization {
        return {
            route: this.route.id,
        };
    }
}
