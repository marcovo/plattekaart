import Location from "../Main/Location";
import Action from "./Action";
import {Serialization} from "../Main/Serializer";

export default class LocationSortAction implements Action {

    private readonly oldIndex: number;

    constructor(
        readonly location: Location,
        readonly newIndex: number,
    ) {
        this.oldIndex = this.location.locationCollection.getLocations().indexOf(location);
    }

    public apply() {
        this.location.locationCollection.moveLocationInList(this.location, this.newIndex);
    }

    public revert() {
        this.location.locationCollection.moveLocationInList(this.location, this.oldIndex);
    }

    public merge(newAction: Action): boolean {
        return false;
    }

    public serializeForDebug(): Serialization {
        return {
            location: this.location.id,
            oldIndex: this.oldIndex,
            newIndex: this.newIndex,
        };
    }
}
