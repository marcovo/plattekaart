import Action from "./Action";
import Route from "../Main/Route";
import {Serialization} from "../Main/Serializer";

export default class RouteAddAction implements Action {

    constructor(readonly route: Route, readonly position: number) {

    }

    public apply() {
        this.route.routeCollection.attachRoute(this.route, this.position);
        this.route.routeCollection.focusRoute(this.route);
    }

    public revert() {
        this.route.routeCollection.unfocus();
        this.route.routeCollection.detachRoute(this.route);

        this.route.routeCollection.checkCloseRouteIntermediatesPanel();
    }

    public merge(newAction: Action): boolean {
        return false;
    }

    public serializeForDebug(): Serialization {
        return {
            route: this.route.id,
            position: this.position,
        };
    }
}
