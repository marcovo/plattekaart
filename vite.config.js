import {defineConfig, loadEnv} from 'vite';
import vue from '@vitejs/plugin-vue';
import {resolve} from "node:path";

export default defineConfig(({command, mode}) => {
    const env = loadEnv(mode, process.cwd(), '');
    return {
        publicDir: '',
        base: './',
        plugins: [
            vue(),
        ],
        resolve: {
            alias: {
                vue: 'vue/dist/vue.esm-bundler.js',
            },
        },
        build: {
            sourcemap: true,
            rollupOptions: {
                input: {
                    main: resolve(__dirname, env.NODE_ENV === 'development' ? 'index.html' : 'plattekaart.html'),
                    changelog: resolve(__dirname, env.NODE_ENV === 'development' ? 'changelog.html' : 'plattekaart-changelog.html'),
                }
            }
        },
        server: {
            host: true,
            hmr: {
                host: 'localhost',
            },
        },
    };
});
