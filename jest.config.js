export default {
    transform: {
        '^.+\\.vue$': '@vue/vue3-jest',
        '^.+\\.js$': 'babel-jest',
        '^.+\\.ts$': 'ts-jest',
    },
    moduleFileExtensions: ['js', 'vue', 'ts'],
    moduleDirectories: [
        "node_modules",
    ],
    transformIgnorePatterns: [],
};
