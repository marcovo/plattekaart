cd /var/www/html/

chmod +x ./docker/wait-for-it/wait-for-it.sh
./docker/wait-for-it/wait-for-it.sh mysql:3306 -- echo 'database initialized'

if test -f "./server/db_structure.sql"; then
  mysql -hmariadb-server-10-2 -uplattekaart -psecret plattekaart < ./server/db_structure.sql
fi
