#!/bin/sh

set -e

echo 'Copy application config'
if test -f "server/config/config.json.example"; then
  cp server/config/config.json.example server/config/config.json
fi

echo 'Trigger init database'
sh ./docker/init-database.sh

echo 'Start apache'
exec apache2-foreground
