#!/usr/bin/env bash

TEST_RELEASE=false

while getopts ":t" opt; do
  case $opt in
    t) TEST_RELEASE=true      ;;
    *) echo 'Available options: -t' >&2
       exit 1
  esac
done

if [[ ! -f release.env ]] ; then
    echo 'File "release.env" not present, aborting.'
    exit
fi

if [ "$TEST_RELEASE" = true ] ; then
  FTP_PATH=$(grep -oP '^FTP_PATH_TEST=\K.*' release.env)
else
  FTP_PATH=$(grep -oP '^FTP_PATH=\K.*' release.env)
fi
FTP_USERNAME=$(grep -oP '^FTP_USERNAME=\K.*' release.env)

echo -n Password:
read -s password

version=$(date +%s)
PLATTEKAART_VERSION=$version make vite-build

rm -rf ./release

cp -r ./dist ./release
cp -r ./src/img/favicon/* ./release/
cp ./site.webmanifest ./release/site.webmanifest
sed -i "s/RELEASE_VERSION_PLACEHOLDER/$version/g" ./release/index.html
sed -i "s/src\/img\/favicon\///g" ./release/index.html
sed -i "s/src\/img\/favicon\///g" ./release/changelog.html
cp ./version.txt ./release/version.txt
sed -i "s/RELEASE_VERSION_PLACEHOLDER/$version/g" ./release/version.txt

cp ./robots.txt ./release/robots.txt
cp ./sitemap.xml ./release/sitemap.xml
sitemap_date=$(date +'%Y-%m-%d')
sed -i "s/LAST_MODIFIED_DATE/$sitemap_date/g" ./release/sitemap.xml
sed -i "s/LAST_MODIFIED_DATE/$sitemap_date/g" ./release/index.html

cd ./release || exit 1;

find . -type f -exec curl -u "$FTP_USERNAME":"$password" --ftp-create-dirs -T {} "$FTP_PATH"{} \;

cd .. || exit 1;

rm -rf ./release
